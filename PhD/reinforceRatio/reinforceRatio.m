%%加固比例统计.
clear
clc

app_id_list = app_id();
app_reinforce_method_ratio_list = app_reinforce_method_ratio();
app_reinforce_instruction_ratio_list = app_reinforce_instruction_ratio();

%单纵坐标轴.
%plot(app_id_list, app_reinforce_ratio_list', '-');
%set(gca,'ytick',[0 0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.2]);
%set(gca,'yticklabel',{'0','1%','2%','3%','4%','5%','6%','7%','8%','9%','10%','11%','12%','13%','14%','15%','16%','17%','18%','19%','20%'});
%title('component amount');
%legend('待保护函数');
%xlabel('应用ID');
%ylabel('待保护函数比例');
%hold on

%双纵坐标轴.
[AX,H1,H2] = plotyy(app_id_list, app_reinforce_method_ratio_list', app_id_list, app_reinforce_instruction_ratio_list', 'plot');

set(AX(1),'XColor','k','YColor','b','XGrid','on','YGrid','on','YLim',[0 0.7],'ytick',[0 0.1 0.2 0.3 0.4 0.5 0.6 0.7],'yticklabel',{'0','10%','20%','30%','40%','50%','60%','70%'});
set(AX(2),'XColor','k','YColor','r','XGrid','on','YGrid','on','YLim',[0 0.7],'ytick',[0 0.1 0.2 0.3 0.4 0.5 0.6 0.7],'yticklabel',{'0','10%','20%','30%','40%','50%','60%','70%'});
%set(AX(2),'XColor','k','YColor','r','YLimMode','auto','ytick',[0 0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.10 0.11 0.12 0.13 0.14 0.15],'yticklabel',{'0','1%','2%','3%','4%','5%','6%','7%','8%','9%','10%','11%','12%','13%','14%','15%'});

HH1=get(AX(1),'Ylabel');
set(HH1,'String','待保护函数比例');
set(HH1,'color','b');

HH2=get(AX(2),'Ylabel');
set(HH2,'String','待保护指令比例');
set(HH2,'color','r');

set(H1,'LineStyle','-');
set(H1,'color','b');
%set(H1,'Marker','x');

set(H2,'LineStyle','-');
set(H2,'color','r');
%set(H2,'Marker','o');

legend([H1,H2],{'待保护函数';'待保护指令'});
xlabel('应用ID');