%%组件影响力评估

clear
clc

%%读取组件数据文件
component_id_list = datainput_component_id();
component_pagerank_list = datainput_component_pagerank();
component_crspr_list = datainput_component_crspr();

%%读取应用数据文件
app_id_list = datainput_app_id();
app_pagerank_list = datainput_app_pagerank();
app_crspr_list = datainput_app_crspr();

%%转置、归一化
component_pagerank_list_normalization = normalization_x(component_pagerank_list');
component_crspr_list_normalization_x = normalization_x(component_crspr_list');
app_pagerank_list_normalization = normalization_x(app_pagerank_list');
app_crspr_list_normalization_x = normalization_x(app_crspr_list');

mAppIdSize=size(app_id_list);
mComponentIdSize=size(component_id_list);
figure(1)

subplot(2,1,1)
plot(component_id_list, component_pagerank_list', '-*', component_id_list, component_crspr_list', '-x');
title('(a)');
legend('PageRanks', 'CRSPR');
xlabel('Component ID');
ylabel('Metrics Value');

subplot(2,1,2)
plot(app_id_list, app_pagerank_list', '-*', app_id_list, app_crspr_list', '-x');
title('(b)');
legend('PageRanks', 'CRSPR');
xlabel('App ID');
ylabel('Metrics Value');
hold on