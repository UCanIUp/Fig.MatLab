clear
clc
%%读取文件
id_list = datainput_id();
betweenness_list = datainput_betweenness();
pagerank_list = datainput_pagerank();
crsmetrics_list = datainput_crsmetrics();

%%转置、归一化
betweenness_list_normalization = normalization_x(betweenness_list');
pagerank_list_normalization = normalization_x(pagerank_list');
crsmetrics_list_normalization_x = normalization_x(crsmetrics_list');
%crsmetrics_list_normalization_y = normalization_y(crsmetrics_list);

mSize=size(id_list);
plot(id_list, betweenness_list_normalization, '-s', id_list, pagerank_list_normalization, '-*', id_list, crsmetrics_list_normalization_x, '-o');

legend('Betweeness Centrality','PageRanks', 'CRSMetrics');
xlabel('Component ID');
ylabel('The Value of Metrics');
