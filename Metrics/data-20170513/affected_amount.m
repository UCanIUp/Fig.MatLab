function y = affected_amount()
affected_amount_list = [
46.83	48.805;
10	43.68;
9	33.67333333;
5	35.82666667;
7	24.4;
5	27.55555556;
4	20.26666667;
4	13.5;
5	24.02222222;
10.25	28.53333333;
];
y = affected_amount_list;
end