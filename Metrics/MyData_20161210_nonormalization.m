%%AC源应用影响力评估
clear
clc

%%读取源应用数据文件
a_source_app_id_list = a_source_app_id();
c_source_app_id_list = c_source_app_id();
a_source_app_crspr_list = a_source_app_crspr();
c_source_app_crspr_list = c_source_app_crspr();

plot(a_source_app_id_list, a_source_app_crspr_list', '-*', c_source_app_id_list, c_source_app_crspr_list', '-x');
%title('CRSPR values of in-link apps');
legend('a', 'c');
xlabel('App ID');
ylabel('Metrics Value');
%hold on