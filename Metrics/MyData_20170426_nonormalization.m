%%crspr应用组件数量统计.
clear
clc

app_id_list = app_id();
app_amount_list = app_amount();

plot(app_id_list, app_amount_list', '-*');
%title('component amount');
legend('a');
xlabel('App ID');
ylabel('Metrics Value');
%hold on