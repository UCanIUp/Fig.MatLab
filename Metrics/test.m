clear all
close all
runoff=[10700 11400 15800 22900 43100 40700 50500 46000 41800 35000];
sed=[0.105 0.094 0.156 1.264   0.363 0.429 0.731 0.682 0.654 0.290];
m=1:10;
[ax,h1,h2]=plotyy(m,runoff,m,sed); %h-- line handle
set(get(ax(1),'Ylabel'),'string','Runoff (m^3/s))','color','r') %y1
set(get(ax(2),'Ylabel'),'string','Sediment concentration (kg/m^3)','color','k') %y2
xlabel('Month')
set(h1,'linestyle','-','color','r');   
set(h2,'linestyle','- -','color','k');
legend([h1 h2],'runoff','sediment concentration') %标注两条线
legend('boxoff')
% box off
set(ax(:),'Ycolor','k') %设定两个Y轴的颜色为黑色
set(ax(1),'ytick',[0:10000:100000]); %设置y轴间隔
set(ax(2),'ytick',[0:0.1:1.5])
set(ax,'xlim',[1 12]) % 设置x轴范围
hold on
scatter(ax(1),4,22900,'r*')
axes(ax(2));
hold on
scatter(4,1.264,'ro')