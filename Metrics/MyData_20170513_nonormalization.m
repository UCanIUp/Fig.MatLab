%%crspr应用组件数量统计.
clear
clc

app_id_list = app_id();
affected_amount_list = affected_amount();

%plot(app_id_list, affected_amount_list', '-*');
bar(affected_amount_list, 1);
%title('The Amount of Affected Apps.');
legend('directly', 'indirectly');
xlabel('ID');
ylabel('Quantity');
set(gca,'ytick',[0 10 20 30 40 50]);
set(gca,'yticklabel',{'0','10','50','500','2000','4000'});
%axis([0 10 0 50]);
%hold on