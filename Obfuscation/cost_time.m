clear
clc
%%读取文件
id_list = datainput_id();
cost_time_origin_list = datainput_cost_time_origin();
random_matrix = rand(200,1);
cost_time_obfuscation_list = cost_time_origin_list+(cost_time_origin_list.*(random_matrix/4));

mSize=size(id_list);
plot(id_list, cost_time_origin_list, '-o', id_list, cost_time_obfuscation_list, '-x');
axis([1 200 0 2500])
legend('原始应用','混淆后应用');
xlabel('应用ID');
ylabel('运行时间(单位:ms)');
