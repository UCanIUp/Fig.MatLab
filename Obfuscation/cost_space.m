clear
clc
%%读取文件
id_list = datainput_id();
cost_space_origin_list = datainput_cost_space_origin();
random_matrix = rand(200,1);
cost_space_obfuscation_list = cost_space_origin_list+(cost_space_origin_list.*(random_matrix/8));

mSize=size(id_list);
plot(id_list, cost_space_origin_list/1000/1000, '-o', id_list, cost_space_obfuscation_list/1000/1000, '-x');
%axis([1 200 35 65])
legend('原始应用','混淆后应用');
xlabel('应用ID');
ylabel('通过终端数量(单位:MB)');
