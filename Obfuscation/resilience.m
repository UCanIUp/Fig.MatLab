clear
clc
%%读取文件
id_list = datainput_id();
resilience_origin_list = datainput_resilience_origin();
resilience_obfuscation_list = datainput_resilience_obfuscation();

%%转置、归一化

mSize=size(id_list);
plot(id_list, resilience_origin_list, '-o', id_list, resilience_obfuscation_list, '-x');
axis([1 200 43 53])
legend('原始应用','混淆后应用');
xlabel('应用ID');
ylabel('通过终端数量');
