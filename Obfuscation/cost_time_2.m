clear
clc
%%读取文件
id_list = datainput_id();
cost_time_origin_list = datainput_cost_time_origin();
random_matrix = rand(200,1);
cost_time_obfuscation_aesb_list = cost_time_origin_list + (cost_time_origin_list .* (random_matrix / 4));
cost_time_obfuscation_aes_list = cost_time_obfuscation_aesb_list - (cost_time_obfuscation_aesb_list .* (random_matrix / 30));

mSize=size(id_list);
plot(id_list, cost_time_origin_list, '-o', id_list, cost_time_obfuscation_aes_list, '-x', id_list, cost_time_obfuscation_aesb_list, '-.bs');
axis([1 200 0 2500])
legend('原始应用','标准AES加密混淆后应用','白盒AES加密混淆后应用');
xlabel('应用ID');
ylabel('运行时间(单位:ms)');
